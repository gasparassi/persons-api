# Pessoas API

## Sobre o projeto
<p>
    Esse aplicativo deverá ser consumido pelo cliente, seja mobile ou web.
</p>

<p>
Se trata de um projeto simples sobre operações básicas dentro um sistema,
 que incluem: criar um registro, ler um ou mais registros, atualizar 
um registro e deletar/remover um registro do banco de dados.
</p>
<p>
    O projeto/API foi desenvolvido em PHP em conjunto com o framework Laravel.
</p>

## Tecnologia
- Core <a href="https://laravel.com">Laravel Framework</a>

## Requirements:

> Framework Laravel 8.65

```
- MySQL >= 5.7
- PHP >= 7.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension
- Docker
```

## Instalação e configuração:

**Passo 1: Git clone**
```
git clone https://gitlab.com/gasparassi/persons-api.git
```
**Passo 2:** Instale os arquivos necessários da pasta vendor:
```
composer install
```

**Passo 3: Criando o banco de dados**

Este software foi desenvolvolvido com a possibilidade de ser utilizado em containers docker.

Na raiz do projeto, existem 2 arquivos: Dockerfile e docker-compose.yml

Para ser executado a instalação do banco de dados utilizando containers docker, 
basta você executar o comando abaixo na raiz do projeto:
```
docker-compose up -d --build 
```
O comando acima irá subir dois containers: um para o aplicativo e outro para o banco
de dados.

Se você não tiver familiaridade com docker, não tem problemas. Basta você configurar
seu arquivo .env com as informações de conexão do seu banco de dados.

**Configurações de conexão com o banco de dados:**

Os passos a seguir são para configurações de conexão com o banco de dados da 
aplicação.
```
1: Crie um novo banco de dados, nomeado-o como, por exemplo: laravel;
2: Copie o arquivo .env.example renomeando-o para .env se o arquivo .env não existir;
3: Gere a chave da API se APP_KEY for null; 
- Use o comando "php artisan key:generate"
4: Valores de configuração do arquivo .env:
- APP_DEBUG=false
- DB_HOST=127.0.0.1 (IP do seu servidor de banco de dados)
- DB_PORT=3306 (Porta de conexão com o servidor de banco de dados MySQL)
- DB_DATABASE=laravel (Nome do banco de dados)
- DB_USERNAME=root (Nome de usuário de acesso ao banco de dados)
- DB_PASSWORD= (Senha do uruário para acesso ao banco de dados)
- APP_URL=http://localhost (Sua url)
```

**Passo 4: Criação das tabelas do banco de dados**

Agora você deverá executar os comando abaixo para a criação das tabelas do banco 
de dados:

`
php artisan migrate
`

Após a criação das tabelas com sucesso, o comando abaixo deverá ser executado 
para a população inicial do banco:

**Passo 5: População das tabelas do banco de dados**

Esse comando executaram duas "seeds" para inserção de valores iniciais no banco
de dados da aplicação.

`
php artisan db:seed
`

Para testar se conferir se tudo está funcionando perfeitamente, basta acessar o 
endereço abaixo para exibir as listagem de todas as categorias cadastradas no banco
 de dados:

`http://localhost:8000/api/v1/categories`

Para exibir todos as pessoas cadastrados no banco de dados com suas respectivas 
categorias, basta acessar o endereço abaixo:

`http://localhost:8000/api/v1/persons`

Os dois endereços acima, quando acessados, exibirão os dados sem formato JSON.
