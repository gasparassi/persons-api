<?php

namespace App\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Person;

interface PersonRepositoryInterface
{
    public function getAll(): LengthAwarePaginator;

    public function getOne(int $id): ?Person;

    public function update(Person $person, array $attributes): Person;

    public function create(array $attributes): ?Person;

    public function delete(Person $person): bool;
}
