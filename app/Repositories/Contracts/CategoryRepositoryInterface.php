<?php

namespace App\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepositoryInterface
{
    public function getAll(): LengthAwarePaginator;
    
    public function getAllForSelect(): Collection;
    
    public function getOne(int $id): ?Category;

    public function update(Category $category, array $attributes): Category;

    public function create(array $attributes): ?Category;

    public function delete(Category $category): bool;
}
