<?php

namespace App\Repositories;

use App\Models\Person;
use App\Repositories\Contracts\PersonRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class PersonRepository implements PersonRepositoryInterface
{

    private Person $entity;

    /**
     * __construct
     *
     * @param  Person $entity
     * @return void
     */
    public function __construct(Person $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Create one resource in database
     *
     * @param  array $attributes
     * @return Person | null
     */
    public function create(array $attributes): ?Person
    {
        return $this->entity->create($attributes);
    }

    /**
     * Get all resources in database
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator
    {
        return $this->entity->paginate();
    }

    /**
     * Get one resource in database
     *
     * @param  int $id
     * @return Person
     */
    public function getOne(int $id): ?Person
    {
        return $this->entity->where('id', $id)->first();
    }

    /**
     * Update one resource in database
     *
     * @param  Person $person
     * @param  array $attributes
     * @return Person
     */
    public function update(Person $person, array $attributes): Person
    {
        $person->fill($attributes);
        $person->save();
        
        return $person->fresh();
    }

    /**
     * Delete one resource in database
     *
     * @param  Person $person
     * @return bool
     */
    public function delete(Person $person): bool
    {
        return $person->delete();
    }
}
