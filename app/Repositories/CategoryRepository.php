<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryRepository implements CategoryRepositoryInterface
{

    private Category $entity;
    
    /**
     * __construct
     *
     * @param  Category $entity
     * @return void
     */
    public function __construct(Category $entity)
    {
        $this->entity = $entity;
    }
    
    /**
     * Get all resources in database
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator
    {
        return $this->entity->paginate();
    }

    public function getAllForSelect(): Collection
    {
        return $this->entity->all();
    }
    
    /**
     * Get one resource in database
     *
     * @param  int $id
     * @return Category
     */
    public function getOne(int $id): ?Category
    {
        return $this->entity->where('id', $id)->first();
    }
    
    /**
     * Update one resource in database
     *
     * @param  Category $category
     * @param  array $attributes
     * @return Category
     */
    public function update(Category $category, array $attributes): Category
    {
        $category->fill($attributes);
        $category->save();
        
        return $category->fresh();
    }
    
    /**
     * Delete one resource in database
     *
     * @param  Category $category
     * @return bool
     */
    public function delete(Category $category): bool
    {
        return $category->delete();
    }
    
    /**
     * Create one resource in database
     *
     * @param  array $attributes
     * @return Category
     */
    public function create(array $attributes): ?Category
    {
        return $this->entity->create($attributes);
    }
}
