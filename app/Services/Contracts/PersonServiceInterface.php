<?php

namespace App\Services\Contracts;

use App\Models\Person;
use Illuminate\Pagination\LengthAwarePaginator;

interface PersonServiceInterface
{
    public function getAll(): LengthAwarePaginator;

    public function getOne(int $id): ?Person;

    public function getOneAndUpdate(array $attributes, int $id): Person;

    public function create(array $attributes): ?Person;

    public function getAndDelete(int $id): bool;
}
