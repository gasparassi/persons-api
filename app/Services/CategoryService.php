<?php

namespace App\Services;

use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Models\Category;
use App\Services\Contracts\CategoryServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryService implements CategoryServiceInterface
{

    private CategoryRepositoryInterface $repository;

    /**
     * __construct
     *
     * @param  mixed $repository
     * @return void
     */
    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * create
     *
     * @param  array $attributes
     * @return Category
     */
    public function create(array $attributes): ?Category
    {
        return $this->repository->create($attributes);
    }

    /**
     * getAll
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator
    {
        return $this->repository->getAll();
    }

    public function getAllForSelect(): Collection
    {
        return $this->repository->getAllForSelect();
    }

    /**
     * getOne
     *
     * @param  int $id
     * @return Category
     */
    public function getOne(int $id): ?Category
    {
        return $this->repository->getOne($id);
    }

    /**
     * getOneAndUpdate
     *
     * @param  array $attributes
     * @param  int $id
     * @return Category
     */
    public function getOneAndUpdate(array $attributes, int $id): Category
    {
        $category = $this->getOne($id);

        if ($category !== null) {
            return $this->repository->update($category, $attributes);
        }

        return false;
    }

    /**
     * getAndDelete
     *
     * @param  int $id
     * @return bool
     */
    public function getAndDelete(int $id): bool
    {
        $category = $this->getOne($id);

        if ($category !== null) {
            return $this->repository->delete($category);
        }

        return false;
    }
}
