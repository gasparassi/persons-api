<?php

namespace App\Services;

use App\Repositories\Contracts\PersonRepositoryInterface;
use App\Models\Person;
use App\Services\Contracts\PersonServiceInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class PersonService implements PersonServiceInterface
{

    private PersonRepositoryInterface $repository;
    
    /**
     * __construct
     *
     * @param  mixed $repository
     * @return void
     */
    public function __construct(PersonRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * create
     *
     * @param  array $attributes
     * @return Person
     */
    public function create(array $attributes): ?Person
    {
        return $this->repository->create($attributes);
    }
    
    /**
     * getAll
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator
    {
        return $this->repository->getAll();
    }
    
    /**
     * getOne
     *
     * @param  int $id
     * @return Person | null
     */
    public function getOne(int $id): ?Person
    {
        return $this->repository->getOne($id);
    }
    
    /**
     * getOneAndUpdate
     *
     * @param  array $attributes
     * @param  int $id
     * @return Person
     */
    public function getOneAndUpdate(array $attributes, int $id): Person
    {
        $person = $this->getOne($id);

        if ($person !== null) {
            return $this->repository->update($person, $attributes);
        }

        return $person;
    }
    
    /**
     * getAndDelete
     *
     * @param  int $id
     * @return bool
     */
    public function getAndDelete(int $id): bool
    {
        $person = $this->getOne($id);

        if ($person !== null) {
            return $this->repository->delete($person);
        }

        return false;
    }
}
