<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PersonResource;
use App\Http\Requests\PersonStoreRequest;
use App\Http\Requests\PersonUpdateRequest;
use App\Services\Contracts\PersonServiceInterface;
use App\Http\Resources\PersonCollection;
use Illuminate\Http\Response;

class PersonController extends Controller
{

    private PersonServiceInterface $service;

    /**
     * @param PersonServiceInterface $service
     */
    public function __construct(PersonServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $persons = $this->service->getAll();
            if (count($persons) > 0) {
                return new PersonCollection($persons);
            } else {
                return response()->json($persons, Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PersonStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonStoreRequest $request)
    {
        try {
            $person = $this->service->create($request->validated());
            if ($person !== null) {
                return new PersonResource($person);
            } else {
                return response()->json($person, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $person = $this->service->getOne($id);
            if ($person !== null) {
                return new PersonResource($person);
            } else {
                return response()->json($person, Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PersonUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonUpdateRequest $request, $id)
    {
        try {
            $person = $this->service->getOneAndUpdate($request->validated(), $id);
            if ($person !== null) {
                return new PersonResource($person);
            } else {
                return response()->json($person, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $person = $this->service->getAndDelete($id);
            if ($person) {
                return response()->json(['data' => 'Pessoa removida com sucesso'], Response::HTTP_OK);
            } else {
                return response()->json(['data' => 'Erro ao remover a Pessoa'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
