<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Services\Contracts\CategoryServiceInterface;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Response;

class CategoryController extends Controller
{

    private CategoryServiceInterface $service;

    /**
     * @param CategoryServiceInterface $service
     */
    public function __construct(CategoryServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $categories = $this->service->getAll();
            if (count($categories) > 0) {
                return new CategoryCollection($categories);
            } else {
                return response()->json($categories, Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexForSelect()
    {
        try {
            $categories = $this->service->getAllForSelect();
            if (count($categories) > 0) {
                return new CategoryCollection($categories);
            } else {
                return response()->json($categories, Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        try {
            $category = $this->service->create($request->validated());
            if ($category !== null) {
                return new CategoryResource($category);
            } else {
                return response()->json($category, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $category = $this->service->getOne($id);
            if ($category !== null) {
                return new CategoryResource($category);
            } else {
                return response()->json($category, Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        try {
            $category = $this->service->getOneAndUpdate($request->validated(), $id);
            if ($category) {
                return new CategoryResource($category);
            } else {
                return response()->json($category, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = $this->service->getAndDelete($id);
            if ($category) {
                return response()->json(['data' => 'Categoria removida com sucesso'], Response::HTTP_OK);
            } else {
                return response()->json(['data' => 'Erro ao remover a categoria'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => 'Erro interno do servidor',
                'message' => $th->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
