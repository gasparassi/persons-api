<?php


namespace App\Http\Requests;

use App\Models\ClientQuota;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;

abstract class FormRequest extends BaseFormRequest
{

    //polymorphic dictorary
    protected array $morphs = [];

    /**
     * Returns the quota of current authenticated client.
     * @return ClientQuota|null
     */
    public function clientQuota() : ?ClientQuota
    {
        return Auth::user()->client->quota;
    }

    /**
     * Validates the document polymorphic relation
     * @param string $key
     * @param string $id
     * @throws AuthorizationException
     */
    public function hasValidPolymorphicRelation(string $key, string $id) : void
    {

        //resolve table
        if(!$table = $this->morphs[$key])
        {
            throw new AuthorizationException("O item vinculado não foi definido.");
        }

        $exists = DB::table($table)->where(
            "id", "=", $id
        )->where(
            "client_id", Auth::user()->client_id
        )->exists();

        if(!$exists)
        {
            throw new AuthorizationException("O item vinculado é inválido.");
        }

    }

    /**
     * Check if client has free quota to store an new user.
     * @param int $claims
     * @throws AuthorizationException
     */
    public function hasFreeUserQuota(int $claims = 1) : void
    {

        if(Auth::user()->isClient() && !$this->clientQuota()->hasAvailableUsers($claims))
        {
            throw new AuthorizationException("O número de usuários livre no plano da empresa é insuficiente.");
        }

        if(Auth::user()->isGroup() && !Auth::user()->group->hasAvailableUsers($claims))
        {
            throw new AuthorizationException("O número de usuários livre no plano do grupo é insuficiente.");
        }

    }

    /**
     * Check if client has free space on quota to upload attachment
     * @param string $key
     * @throws AuthorizationException
     */
    public function hasFreeStorageQuotaForAttachments(string $key = "attachment") : void
    {

        $file = $this->file($key);

        if($file && !$this->clientQuota()->hasAvailableStorage($file->getSize()))
        {
            throw new AuthorizationException("O espaço de armazenamento de arquivos livre no plano é insuficiente.");
        }

    }

}
