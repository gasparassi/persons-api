<?php

namespace Database\Seeders;

use App\Models\Person;
use Illuminate\Database\Seeder;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Person::query()->create([
            'name' => 'Jorge da Silva',
            'email' => 'jorge@terra.com.br',
            'category_id' => 1
        ]);
        Person::query()->create([
            'name' => 'Flavia Monteiro',
            'email' => 'flavia@globo.com',
            'category_id' => 2
        ]);
        Person::query()->create([
            'name' => 'Marcos Frota Ribeiro',
            'email' => 'ribeiro@gmail.com',
            'category_id' => 2
        ]);
        Person::query()->create([
            'name' => 'Raphael Souza Santos',
            'email' => 'rsantos@gmail.com',
            'category_id' => 1
        ]);
        Person::query()->create([
            'name' => 'Pedro Paulo Mota',
            'email' => 'ppmota@gmail.com',
            'category_id' => 1
        ]);
        Person::query()->create([
            'name' => 'Winder Carvalho da Silva',
            'email' => 'winder@hotmail.comr',
            'category_id' => 3
        ]);
        Person::query()->create([
            'name' => 'Maria da Penha Albuquerque',
            'email' => 'mpa@hotmail.com',
            'category_id' => 3
        ]);
        Person::query()->create([
            'name' => 'Rafael Garcia Souza',
            'email' => 'rgsouza@hotmail.com',
            'category_id' => 3
        ]);
        Person::query()->create([
            'name' => 'Tabata Costa',
            'email' => 'tabata_costa@gmail.com',
            'category_id' => 2
        ]);
        Person::query()->create([
            'name' => 'Ronil Camarote',
            'email' => 'camarote@terra.com.br',
            'category_id' => 1
        ]);
        Person::query()->create([
            'name' => 'Joaquim Barbosa',
            'email' => 'barbosa@globo.com',
            'category_id' => 1
        ]);
        Person::query()->create([
            'name' => 'Eveline Maria Alcantra',
            'email' => 'ev_alcantra@gmail.com',
            'category_id' => 2
        ]);
        Person::query()->create([
            'name' => 'João Paulo Vieira',
            'email' => 'jpvieria@gmail.com',
            'category_id' => 1
        ]);
        Person::query()->create([
            'name' => 'Carla Zamborlini',
            'email' => 'zamborlini@terra.com.br',
            'category_id' => 3
        ]);
    }
}
