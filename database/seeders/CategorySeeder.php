<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::query()->create(['name' => 'Admin']);
        Category::query()->create(['name' => 'Gerente']);
        Category::query()->create(['name' => 'Normal']);
    }
}
